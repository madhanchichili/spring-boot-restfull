package com.rest.controller;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.rest.Model.Student;
import com.rest.Model.bcakend.StudentRegistrationReply;
import com.rest.bean.StudentRegistration;




	@RestController
	public class StudentRetrieveController  {
		
		@RequestMapping(method = RequestMethod.GET, value="/student/allstudent")
		@ResponseBody
		public List<Student> getAllStudents() {
			  return StudentRegistration.getInstance().getStudentRecords();
			  }
		
		
		@RequestMapping(method = RequestMethod.POST, value="/register/student")
		 public StudentRegistrationReply registerStudent(@RequestBody Student student) {
			 
			 StudentRegistrationReply stdregreply = new StudentRegistrationReply();  
			 
			 StudentRegistration.getInstance().add(student);
			 stdregreply.setName(student.getName());
			 stdregreply.setAge(student.getAge());
			 stdregreply.setRegistrationNumber(student.getRegistrationNumber());
			 stdregreply.setRegistrationStatus("Sucessful");
			return stdregreply;
			 
		 }
}