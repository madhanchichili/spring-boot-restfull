package com.rest.bean;

import java.util.ArrayList;
import java.util.List;

import com.rest.Model.Student;

public class StudentRegistration  {

	   private List<Student> studentRecords;
	   
	   private static  StudentRegistration stdregd  = null;
	   
	   private StudentRegistration()
	   {
		   studentRecords = new ArrayList<Student>();
	   }
	   public static  StudentRegistration getInstance()
	   {
		   if(stdregd == null)
		   {
			   stdregd = new StudentRegistration();
			   return stdregd;
		   }
		
		   else return stdregd;
	   }
	   public void add(Student std)
	   {
		   studentRecords.add(std);
	   }
	   public String updateStudent(Student std)
	   {
		   for (int i = 0;i<studentRecords.size();i++)
		   {
			   Student sdn = studentRecords.get(i);
			   if(sdn.getRegistrationNumber().equals(std.getRegistrationNumber()))
			   {
				   studentRecords.set(i, std);
				   return "Update successful";
			   }
		   }
		return "unsucessful";
	   }
	   
	   public String deleteStudent(Student std)
	   {
		   for (int i = 0;i<studentRecords.size();i++)
		   {
			   Student sdn = studentRecords.get(i);
			   if(sdn.getRegistrationNumber().equals(std.getRegistrationNumber()))
			   {
				   studentRecords.remove(i);
				   return "delete is sucessful";
			   }
		   }
		return  "delete is unsucessful";
		   
	   }
	   public List<Student>  getStudentRecords()
	   {
		return studentRecords;
		   
	   }
}
